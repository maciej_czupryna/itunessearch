//
//  MediaElementsProviderProtocol.swift
//  App
//
//  Created by Maciej Czupryna on 20.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Model

struct MediaElementsFilter {
    var term: String
    var country: String
}

enum ArtworkSize {
    case small
    case large
}

protocol MediaElementsProviderProtocol {
    func getMediaElements(_ filter: MediaElementsFilter) -> Observable<[MediaEntity]>
    func getArtworkImage(_ url: URL?, artworkSize: ArtworkSize) -> Driver<UIImage>
}
