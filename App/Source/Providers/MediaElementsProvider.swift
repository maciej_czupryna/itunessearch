//
//  MediaElementsProvidere.swift
//  App
//
//  Created by Maciej Czupryna on 20.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Api
import Model
import Utilities

class MediaElementsProvider: MediaElementsProviderProtocol {

    let api: Api

    init(api: Api) {
        self.api = api
    }

    func getMediaElements(_ filter: MediaElementsFilter) -> Observable<[MediaEntity]> {
        let apiQuery = ApiQuery(term: filter.term, country: filter.country, media: "podcast", entity: "podcast")
        return api.search(apiQuery)
    }

    func getArtworkImage(_ url: URL?, artworkSize: ArtworkSize) -> Driver<UIImage> {
        guard let image = UIImage(named: artworkSize.placeholderImageName) else {
            fatalError("No placeholder image found")
        }
        guard let url = url else {
            return Driver.just(image)
        }
        if let cacheImage = ImageCache.sharedCache.object(forKey: url.absoluteString as NSString) {
            return Driver.just(cacheImage)
        }
        return api.image(url).do(onNext: { (image) in
            ImageCache.sharedCache.setObject(image, forKey: url.absoluteString as NSString)
        }, onError: nil, onCompleted: nil, onSubscribe: nil, onSubscribed: nil, onDispose: nil).asDriver(onErrorJustReturn: image).startWith(image)
    }
}

extension ArtworkSize {
    var placeholderImageName: String {
        switch self {
        case .large:
            return "placeholder_image"
        case .small:
            return "placeholder_cell"
        }
    }
}
