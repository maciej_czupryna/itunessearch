//
//  DetailsViewModel.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import AppStructure
import Model

final class DetailsViewModel: ViewModel {

    // Output (call bind() before subscribing)
    private(set) var artworkImage: Driver<UIImage>!
    private(set) var title: Driver<String>!
    private(set) var artistName: Driver<String>!
    private(set) var releaseDate: Driver<Date?>!
    private(set) var genres: Driver<[String]?>!

    private let router: DetailsRouterProtocol
    private let provider: MediaElementsProviderProtocol
    private let mediaEntity: Variable<MediaEntity>

    init(router: DetailsRouterProtocol, mediaEntity: MediaEntity, provider: MediaElementsProviderProtocol) {
        self.router = router
        self.mediaEntity = Variable<MediaEntity>(mediaEntity)
        self.provider = provider
        super.init()
    }

    func bind() {
        self.title = self.mediaEntity.asObservable().map({ (entity) -> String in
            return entity.trackName
        }).asDriver(onErrorJustReturn: "")

        self.artistName = self.mediaEntity.asObservable().map({ (entity) -> String in
            return entity.artistName
        }).asDriver(onErrorJustReturn: "")

        self.releaseDate = self.mediaEntity.asObservable().map({ (entity) -> Date? in
            return entity.releaseDate
        }).asDriver(onErrorJustReturn: nil)

        self.genres = self.mediaEntity.asObservable().map({ (entity) -> [String]? in
            return entity.genres
        }).asDriver(onErrorJustReturn: nil)

        self.artworkImage = self.mediaEntity.asDriver().flatMap({ [provider] (entity) -> SharedSequence<DriverSharingStrategy, UIImage> in
            return provider.getArtworkImage(entity.artworkUrl600, artworkSize: .large)
        })
    }
}
