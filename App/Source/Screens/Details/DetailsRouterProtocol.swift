//
//  DetailsRouterProtocol.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import Model

protocol DetailsRouterProtocol {
    func goBack()
}
