//
//  DetailsRouter.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import UIKit
import AppStructure
import Utilities
import Model
import ITunesApi

final class DetailsRouter: Router {

    static func instantiateViewController(navigationController: UINavigationController, mediaEntity: MediaEntity) -> DetailsViewController {
        let router = DetailsRouter(navigationController: navigationController)
        let viewModel = ViewModelFactory.shared.detailsViewModel(router: router, mediaEntity: mediaEntity)
        let viewController = DetailsViewController.instantiateFromStoryboard(viewModel: viewModel)
        return viewController
    }

    static func show(navigationController: UINavigationController, mediaEntity: MediaEntity, animated: Bool = true) {
        navigationController.pushViewController(instantiateViewController(navigationController: navigationController, mediaEntity: mediaEntity), animated: animated)
    }

    static func showAsRoot(navigationController: UINavigationController, mediaEntity: MediaEntity, animated: Bool = false) {
        navigationController.setViewControllers([instantiateViewController(navigationController: navigationController, mediaEntity: mediaEntity)], animated: animated)
    }
}

extension DetailsRouter: DetailsRouterProtocol {
    func goBack() {
        navigationController.popViewController(animated: true)
    }
}
