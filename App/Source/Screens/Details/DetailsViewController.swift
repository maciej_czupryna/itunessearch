//
//  DetailsViewController.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import UIKit
import AppStructure
import Utilities
import Model
import RxSwift
import RxCocoa

final class DetailsViewController: ViewController<DetailsViewModel> {

    @IBOutlet private weak var artworkImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var releaseDateLabel: UILabel!
    @IBOutlet private weak var genresLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = LocalizedString(key: "details_title")

        bindComponents()
    }

    private func bindComponents() {
        viewModel.bind()

        viewModel.artworkImage.drive(artworkImageView.rx.image).disposed(by: disposeBag)
        viewModel.title.drive(titleLabel.rx.text).disposed(by: disposeBag)
        viewModel.artistName.drive(authorLabel.rx.text).disposed(by: disposeBag)
        viewModel.releaseDate.map { (date) -> String? in
            guard let date = date else {
                return nil
            }
            return DateFormatters.UI.releaseDateFormatter.string(from: date)
        }.drive(releaseDateLabel.rx.text).disposed(by: disposeBag)
        viewModel.genres.map { (genres) -> String? in
            return genres?.joined(separator: ", ")
        }.drive(genresLabel.rx.text).disposed(by: disposeBag)
    }
}
