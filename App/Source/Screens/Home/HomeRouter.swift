//
//  HomeRouter.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import UIKit
import AppStructure
import Utilities
import ITunesApi
import Model

final class HomeRouter: Router {

    static func instantiateViewController(navigationController: UINavigationController) -> HomeViewController {
        let router = HomeRouter(navigationController: navigationController)
        let viewModel = ViewModelFactory.shared.homeViewModel(router: router)
        let viewController = HomeViewController.instantiateFromStoryboard(viewModel: viewModel)
        return viewController
    }

    static func show(navigationController: UINavigationController, animated: Bool = true) {
        navigationController.pushViewController(instantiateViewController(navigationController: navigationController), animated: animated)
    }

    static func showAsRoot(navigationController: UINavigationController, animated: Bool = false) {
        navigationController.setViewControllers([instantiateViewController(navigationController: navigationController)], animated: animated)
    }
}

extension HomeRouter: HomeRouterProtocol {
    func goToDetails(_ mediaEntity: MediaEntity) {
        DetailsRouter.show(navigationController: navigationController, mediaEntity: mediaEntity)
    }
}
