//
//  HomeHomeViewModel.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import AppStructure
import Model
import Utilities

final class HomeViewModel: ViewModel {

    // Output (call bind() before subscribing)
    private(set) var elements: Driver<[MediaEntity]>!
    private(set) var isBusy: Driver<Bool>!
    private(set) var resultsInfoText: Driver<String>!
    private(set) var failure: Observable<Error>!

    private let router: HomeRouterProtocol
    private let provider: MediaElementsProviderProtocol
    private var elementsList = Variable<[MediaEntity]>([])

    init(provider: MediaElementsProviderProtocol, router: HomeRouterProtocol) {
        self.provider = provider
        self.router = router
        super.init()
    }

    func bind(input: (
        searchText: Driver<String>,
        selectAction: Driver<IndexPath>
    )) {
        let searchText = input.searchText
            .skip(1)
            .distinctUntilChanged({ (left, right) -> Bool in
                return left == right
            })
        // Failure
        let failureSubject = PublishSubject<Error>()
        failure = failureSubject.asObservable()

        // Search
        let search = searchText.flatMapLatest { [provider] (text) -> SharedSequence<DriverSharingStrategy, [MediaEntity]> in
            let filter = MediaElementsFilter(term: text, country: "PL")
            return provider.getMediaElements(filter).asDriver(onErrorRecover: { (error) -> SharedSequence<DriverSharingStrategy, [MediaEntity]> in
                failureSubject.onNext(error)
                return Driver.just([])
            })
        }
        elements = search.startWith([])
        search.drive(elementsList).disposed(by: disposeBag)

        // Is busy
        isBusy = Driver.from([
                searchText.map { _  in true },
                search.map { _  in false }
            ])
            .merge()
            .startWith(false)

        // Result info
        resultsInfoText = Driver.combineLatest(
                searchText.map { (text) -> Bool in
                    return !text.isEmpty
                },
                elements.map { (entities) -> Bool in
                    return entities.isEmpty
                },
                isBusy.map { (val) -> Bool in
                    return !val
                }
            ).map {
                ($0 && $1 && $2) ? LocalizedString(key: "results_empty") : ""
            }.startWith("")

        // Actions
        input.selectAction.drive(onNext: { [router, elementsList] (indexPath) in
            router.goToDetails(elementsList.value[indexPath.row])
            }, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)
    }
}
