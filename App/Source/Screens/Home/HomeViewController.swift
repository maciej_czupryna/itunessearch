//
//  HomeViewController.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import UIKit
import AppStructure
import Utilities
import Model
import RxSwift
import RxCocoa

final class HomeViewController: ViewController<HomeViewModel> {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var resultsInfoLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = LocalizedString(key: "home_title")
        searchBar.placeholder = LocalizedString(key: "search_placeholder")
        tableView.tableHeaderView = searchBar
        tableView.register(HomeTableViewCell.self)

        bindComponents()
    }

    private func bindComponents() {
        viewModel.bind(input: (
            searchText: searchBar.rx.text.orEmpty.asDriver().debounce(0.7),
            selectAction: tableView.rx.itemSelected.asDriver()
        ))

        // Bind table view data source
        viewModel.elements
            .drive(tableView.rx.items(cellIdentifier: HomeTableViewCell.reuseIdentifier, cellType: HomeTableViewCell.self)) { (row, element, cell) in
                let cellViewModel = ViewModelFactory.shared.homeCellViewModel(mediaEntity: element)
                cell.setViewModel(cellViewModel)
        }.disposed(by: disposeBag)

        viewModel.resultsInfoText.drive(resultsInfoLabel.rx.text).disposed(by: disposeBag)

        searchBar.rx.searchButtonClicked.subscribe(onNext: { [weak self] () in
            self?.searchBar.resignFirstResponder()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)

        // Bind activity indicator
        viewModel.isBusy.asObservable().subscribe(onNext: { [weak self] (isBusy) in
            isBusy ? self?.showProgress(animated: true) : self?.hideProgress(animated: true)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)

        // Bind on error
        viewModel.failure.subscribe(onNext: { [weak self] (error) in
            self?.alert(with: LocalizedString(key: "error"), message: error.localizedDescription)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
}
