//
//  HomeTableViewCellViewModel.swift
//  App
//
//  Created by Maciej Czupryna on 22.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Model

final class HomeTableViewCellViewModel {

    // Output
    let title: Driver<String>
    let artistname: Driver<String>
    let releaseDate: Driver<Date?>
    let artworkImage: Driver<UIImage>
    let disposeBag = DisposeBag()

    private let provider: MediaElementsProviderProtocol
    private let mediaEntity: Variable<MediaEntity>

    init(provider: MediaElementsProviderProtocol, mediaEntity: MediaEntity) {
        self.provider = provider
        self.mediaEntity = Variable(mediaEntity)

        self.title = self.mediaEntity.asDriver().map { (entity) -> String in
            return entity.trackName
        }

        self.artistname = self.mediaEntity.asDriver().map { (entity) -> String in
            return entity.artistName
        }

        self.releaseDate = self.mediaEntity.asDriver().map { (entity) -> Date? in
            return entity.releaseDate
        }
        
        self.artworkImage = self.mediaEntity.asDriver().flatMap({ (entity) -> SharedSequence<DriverSharingStrategy, UIImage> in
            return provider.getArtworkImage(entity.artworkUrl100, artworkSize: .small)
        })
    }
}
