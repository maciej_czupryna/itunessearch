//
//  HomeTableViewCell.swift
//  App
//
//  Created by Maciej Czupryna on 22.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import UIKit
import RxSwift
import Utilities

class HomeTableViewCell: UITableViewCell, NibLoadableView, ReusableView {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!

    private var viewModel: HomeTableViewCellViewModel!

    func setViewModel(_ viewModel: HomeTableViewCellViewModel) {
        self.viewModel = viewModel

        viewModel.title.drive(titleLabel.rx.text).disposed(by: viewModel.disposeBag)
        viewModel.artistname.drive(detailsLabel.rx.text).disposed(by: viewModel.disposeBag)
        viewModel.releaseDate.map { (date) -> String? in
            guard let date = date else {
                return nil
            }
            return DateFormatters.UI.releaseDateFormatter.string(from: date)
        }.drive(dateLabel.rx.text).disposed(by: viewModel.disposeBag)
        viewModel.artworkImage.drive(iconView.rx.image).disposed(by: viewModel.disposeBag)
    }
}
