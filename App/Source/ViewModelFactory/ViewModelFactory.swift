//
//  ViewModelFactory.swift
//  App
//
//  Created by Maciej Czupryna on 22.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import Api
import ITunesApi
import Model

final class ViewModelFactory {

    static let shared = ViewModelFactory()

    private let api: Api

    init() {
        self.api = ITunesApi()
    }

    func homeViewModel(router: HomeRouter) -> HomeViewModel {
        let provider = MediaElementsProvider(api: api)
        return HomeViewModel(provider: provider, router: router)
    }

    func detailsViewModel(router: DetailsRouter, mediaEntity: MediaEntity) -> DetailsViewModel {
        let provider = MediaElementsProvider(api: api)
        return DetailsViewModel(router: router, mediaEntity: mediaEntity, provider: provider)
    }

    func homeCellViewModel(mediaEntity: MediaEntity) -> HomeTableViewCellViewModel {
        let provider = MediaElementsProvider(api: api)
        return HomeTableViewCellViewModel(provider: provider, mediaEntity: mediaEntity)
    }
}
