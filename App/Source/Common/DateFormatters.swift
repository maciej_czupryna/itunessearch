//
//  DateFormatters.swift
//  App
//
//  Created by Maciej Czupryna on 22.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

struct DateFormatters {
    struct UI {
        static let releaseDateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .long
            formatter.timeStyle = .medium
            return formatter
        }()
    }
}
