//
//  Application.swift
//  App
//
//  Created by Maciej Czupryna on 06.12.2016.
//  Copyright © 2016 The Software House. All rights reserved.
//

import Foundation
import AppStructure
import UIKit

/// Class manages application lifecycle. Created to move a logic from appDelegate.
final class ApplicationRouter: Router {
    
    func showFirstScreen() {
        HomeRouter.showAsRoot(navigationController: navigationController)
    }
    
}
