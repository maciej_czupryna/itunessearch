//
//  ViewModel.swift
//  App
//
//  Created by Maciej Czupryna on 08.09.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

open class ViewModel: ViewModelProtocol {

    public let disposeBag = DisposeBag()
    public let onLoad = PublishSubject<Void>()

    public init() {
        
    }
}
