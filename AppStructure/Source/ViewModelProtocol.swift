//
//  ViewModelProtocol.swift
//  App
//
//  Created by Maciej Czupryna on 13.02.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public protocol ViewModelProtocol {
    /// Notify view model that view did load
    var onLoad: PublishSubject<Void> { get }
}
