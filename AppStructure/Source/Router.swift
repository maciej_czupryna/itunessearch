//
//  Router.swift
//  App
//
//  Created by Maciej Czupryna on 02.11.2016.
//  Copyright © 2016 The Software House. All rights reserved.
//

import Foundation
import UIKit

open class Router: NSObject {
    
    public fileprivate(set) weak var navigationController: UINavigationController!
    
    required public init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
}
