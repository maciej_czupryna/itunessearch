//
//  ViewController+Storyboard.swift
//  App
//
//  Created by Maciej Czupryna on 06.09.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import UIKit

extension ViewController {

    public class func instantiateFromStoryboard(viewModel: T) -> Self {
        return self.instantiateFromStoryboard(self, viewModel: viewModel)
    }

    fileprivate class func instantiateFromStoryboard<T: ViewController<V>, V>(_ type: T.Type, viewModel: V) -> T {
        let name = String(describing: self).replacingOccurrences(of: "ViewController", with: "")

        let storyboard = UIStoryboard(name: name+"Storyboard", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: name) as? T else {
            fatalError()
        }
        vc.viewModel = viewModel
        return vc
    }
}
