//
//  ViewController.swift
//  App
//
//  Created by Maciej Czupryna on 12.12.2016.
//  Copyright © 2016 The Software House. All rights reserved.
//

import UIKit
import Utilities
import RxSwift

open class ViewController<T: ViewModelProtocol>: UIViewController {

    public let disposeBag = DisposeBag()

    public var viewModel: T!

    public init() {
        super.init(nibName: nil, bundle: nil)
        onInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        onInit()
    }
    
    open func onInit() {
        // Override this method in your view controller for initialization
    }

    override open func viewDidLoad() {
        super.viewDidLoad()
        viewModel.onLoad.onNext(())
    }
}

extension ViewController: AlertingProtocol, ProgressProtocol {

}
