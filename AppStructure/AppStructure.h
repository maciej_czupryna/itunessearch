//
//  AppStructure.h
//  AppStructure
//
//  Created by Maciej Czupryna on 06.09.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AppStructure.
FOUNDATION_EXPORT double AppStructureVersionNumber;

//! Project version string for AppStructure.
FOUNDATION_EXPORT const unsigned char AppStructureVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AppStructure/PublicHeader.h>


