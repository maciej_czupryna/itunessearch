//
//  UIViewController+Rx.swift
//  App
//
//  Created by Maciej Czupryna on 08.09.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift

extension Reactive where Base: UIViewController {
    var viewDidLoad: Observable<Void> {
        return self.sentMessage(#selector(Base.viewDidLoad)).map { _ in Void() }
    }
}
