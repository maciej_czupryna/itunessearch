//
//  Utilities.h
//  Utilities
//
//  Created by Maciej Czupryna on 06.12.2016.
//  Copyright © 2016 The Software House. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Utilities.
FOUNDATION_EXPORT double UtilitiesVersionNumber;

//! Project version string for Utilities.
FOUNDATION_EXPORT const unsigned char UtilitiesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Utilities/PublicHeader.h>


