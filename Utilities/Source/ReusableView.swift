//
//  ReusableView.swift
//  App
//
//  Created by Maciej Czupryna on 23.06.2016.
//  Copyright © 2016 The Software House. All rights reserved.
//

import Foundation
import UIKit

public protocol ReusableView: class { }

extension ReusableView where Self: UIView {

    public static var reuseIdentifier: String {
        return String(describing: self)
    }

}
