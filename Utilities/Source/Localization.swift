//
//  Localization.swift
//  App
//
//  Created by Maciej Czupryna on 08.12.2016.
//  Copyright © 2016 The Software House. All rights reserved.
//

import Foundation

public func LocalizedString(key: String) -> String {
    return Localization.shared.localizedString(key: key)
}

public class Localization {
    
    public static let shared = Localization()
    
    public func localizedString(key: String, comment: String = "") -> String {
        return NSLocalizedString(key, comment: comment)
    }
}
