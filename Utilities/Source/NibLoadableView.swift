//
//  NibLoadableView.swift
//  App
//
//  Created by Maciej Czupryna on 28.09.2016.
//  Copyright © 2016 The Software House. All rights reserved.
//

import Foundation
import UIKit

public protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    
    public static var nibName: String {
        return String(describing: self)
    }
    
}
