//
//  ImageCache.swift
//  Utilities
//
//  Created by Maciej Czupryna on 22.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

public final class ImageCache {

    public static let sharedCache: NSCache<NSString, UIImage> = {
        let cache = NSCache<NSString, UIImage>()
        cache.name = "ImageCache"
        cache.countLimit = 500
        cache.totalCostLimit = 50*1024*1024
        return cache
    }()
}
