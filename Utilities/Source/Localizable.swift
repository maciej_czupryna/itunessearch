//
//  Localizable.swift
//  App
//
//  Created by Maciej Czupryna on 08.09.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

public protocol Localizable {
    var localizedString: String? { get }
}
