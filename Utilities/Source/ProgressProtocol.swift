//
//  ProgressProtocol.swift
//  App
//
//  Created by Maciej Czupryna on 12.01.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import SVProgressHUD

public protocol ProgressProtocol {
    
}

extension ProgressProtocol where Self: UIViewController {
    
    public func showProgress(animated: Bool = false) {
        SVProgressHUD.show()
    }
    
    public func hideProgress(animated: Bool = false) {
        SVProgressHUD.dismiss()
    }
}
