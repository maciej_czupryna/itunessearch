//
//  AlertingProtocol.swift
//  App
//
//  Created by Maciej Czupryna on 03.01.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

public protocol AlertingProtocol {
    
}

extension AlertingProtocol where Self: UIViewController {

    public func alert(with title: String?, message: String?, actions: [UIAlertAction]? = nil, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let actions = actions {
            for action in actions {
                alert.addAction(action)
            }
        } else {
            let action = UIAlertAction(title: LocalizedString(key: "alert.ok"), style: .cancel, handler: nil)
            alert.addAction(action)
        }
        present(alert, animated: true, completion: completion)
    }
}
