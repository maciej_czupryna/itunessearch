//
//  MediaElementsProviderTests.swift
//  AppTests
//
//  Created by Maciej Czupryna on 23.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxCocoa
import Quick
import Nimble
@testable import App
@testable import Model

class MediaElementsProviderTests: QuickSpec {
    override func spec() {
        var mockApi: ApiMock!
        var provider: MediaElementsProviderProtocol!
        let disposeBag = DisposeBag()

        beforeEach {
            mockApi = ApiMock()
            provider = MediaElementsProvider(api: mockApi)
        }

        describe("On search, constructed query") {
            beforeEach {
                mockApi.entitiesToReturn = []
            }
            it("shoud not be nil", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    let phrase = "Some phrase"
                    let country = "US"
                    let filter = MediaElementsFilter(term: phrase, country: country)
                    provider.getMediaElements(filter).subscribe(onNext: { (list) in
                        expect(mockApi.apiQueryReceived).notTo(beNil())
                        done()
                    }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                })
            })

            it("shoud generate proper search values", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    let phrase = "Some phrase"
                    let country = "US"
                    let filter = MediaElementsFilter(term: phrase, country: country)
                    provider.getMediaElements(filter).subscribe(onNext: { (list) in
                        expect(mockApi.apiQueryReceived!.term).to(equal(phrase))
                        expect(mockApi.apiQueryReceived!.country).to(equal(country))
                        done()
                    }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                })
            })
        }

        describe("On found results") {
            beforeEach {
                mockApi.entitiesToReturn = MediaEntity.mockNotEmpty3()
            }

            it("should return list", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    let filter = MediaElementsFilter(term: "Some phrase", country: "US")
                    provider.getMediaElements(filter).subscribe(onNext: { (list) in
                        expect(list).to(haveCount(3))
                        done()
                    }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                })
            })
        }

    }
}
