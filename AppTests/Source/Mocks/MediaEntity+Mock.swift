//
//  MediaEntity+Mock.swift
//  AppTests
//
//  Created by Maciej Czupryna on 22.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import Model

extension MediaEntity {
    static func mockNotEmpty3() -> [MediaEntity] {
        return [
            mock(), mock(), mock()
        ]
    }

    static func mock() -> MediaEntity {
        return MediaEntity(trackId: 1, trackName: "Some name", artistName: "Some artist", artworkUrl100: URL(string: "https://google.com"), artworkUrl600: URL(string: "https://google.com"), genres: ["First", "Second"], releaseDate: Date.distantPast)
    }
}
