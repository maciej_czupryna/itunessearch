//
//  HomeHomeRouterMock.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import XCTest
@testable import App
@testable import Model

class HomeRouterMock: HomeRouterProtocol {

    var mediaEntity: MediaEntity?
    var didGoToDetails = false

    func goToDetails(_ mediaEntity: MediaEntity) {
        self.mediaEntity = mediaEntity
    }
}
