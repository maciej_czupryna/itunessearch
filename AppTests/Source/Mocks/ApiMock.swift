//
//  ApiMock.swift
//  AppTests
//
//  Created by Maciej Czupryna on 23.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift
@testable import Model
@testable import Api

final class ApiMock: Api {

    var imageToReturn: UIImage!
    var entitiesToReturn: [MediaEntity]!
    var apiQueryReceived: ApiQuery?

    func search(_ query: ApiQuery) -> Observable<[MediaEntity]> {
        apiQueryReceived = query
        return Observable.just(entitiesToReturn).delay(0.1, scheduler: MainScheduler.instance)
    }

    func image(_ url: URL) -> Observable<UIImage> {
        return Observable.just(imageToReturn).delay(0.1, scheduler: MainScheduler.instance)
    }
}
