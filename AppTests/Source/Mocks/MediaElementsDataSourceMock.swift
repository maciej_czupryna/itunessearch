//
//  MediaElementsProviderMock.swift
//  AppTests
//
//  Created by Maciej Czupryna on 22.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
@testable import App
@testable import Model

final class MediaElementsProviderMock: MediaElementsProviderProtocol {
    var entitiesToReturn: [MediaEntity] = []
    var imageToReturn: UIImage!
    var errorToReturn: Error?

    func getMediaElements(_ filter: MediaElementsFilter) -> Observable<[MediaEntity]> {
        if let error = errorToReturn {
            return Observable.error(error)
        }
        return Observable.just(entitiesToReturn).delay(0.1, scheduler: MainScheduler.instance)
    }

    func getArtworkImage(_ url: URL?, artworkSize: ArtworkSize) -> Driver<UIImage> {
        return Driver.just(imageToReturn).delay(0.1)
    }
}
