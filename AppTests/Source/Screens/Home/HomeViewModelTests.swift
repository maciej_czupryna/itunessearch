//
//  HomeHomeViewModelTests.swift
//  App
//
//  Created by mczupryna on 13/02/2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import Quick
import Nimble
@testable import App
@testable import Model

class HomeViewModelTests: QuickSpec {

    override func spec() {
        var viewModel: HomeViewModel!
        var mockProvider: MediaElementsProviderMock!
        var mockRouter: HomeRouterMock!
        var disposeBag: DisposeBag!
        // Input signals
        var textInput: Variable<String>!
        var selectionInput: PublishSubject<IndexPath>!

        beforeEach {
            disposeBag = DisposeBag()
            mockRouter = HomeRouterMock()
            mockProvider = MediaElementsProviderMock()
            viewModel = HomeViewModel(provider: mockProvider, router: mockRouter)
            // Control events
            textInput = Variable("")
            selectionInput = PublishSubject()
            viewModel.bind(input: (
                searchText: textInput.asDriver(),
                selectAction: selectionInput.asDriver(onErrorJustReturn: IndexPath(row: 99, section: 99))
            ))
        }

        describe("On no text input", closure: {
            it("should have empty data set", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    viewModel.elements.drive(onNext: { (entities) in
                        expect(entities).to(beEmpty())
                        done()
                    }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                })
            })

            it("should not be busy", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    viewModel.isBusy.drive(onNext: { (busy) in
                        expect(busy).to(beFalse())
                        done()
                    }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                })
            })
        })

        describe("On some text input", closure: {
            beforeEach {
                mockProvider.entitiesToReturn = MediaEntity.mockNotEmpty3()
            }
            it("should have elements in data set", closure: {
                waitUntil(timeout: 10, action: { (done) in
                    viewModel.elements.skip(1).drive(onNext: { (entities) in
                        expect(entities).to(haveCount(3))
                        done()
                    }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                    textInput.value = "Test"
                })
            })

            it("should be busy", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    viewModel.isBusy.skip(1).drive(onNext: { (busy) in
                        expect(busy).to(beTrue())
                        done()
                    }, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                    textInput.value = "Test"
                })
            })

            it("should not be busy after data received", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    viewModel.isBusy.skip(2).asObservable().take(1).subscribe(onNext: { (busy) in
                        expect(busy).to(beFalse())
                        done()
                    }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                    textInput.value = "Test"
                })
            })
        })

        describe("On network error") {
            enum MockError: Error {
                case SomeError
            }
            beforeEach {
                mockProvider.errorToReturn = MockError.SomeError
            }
            it("should return error", closure: {
                waitUntil(timeout: 2, action: { (done) in
                    viewModel.failure.subscribe(onNext: { (error) in
                        expect(error).to(matchError(MockError.SomeError))
                        done()
                    }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
                    textInput.value = "Test"
                })
            })
        }

    }
}
