//
//  Podcast.swift
//  Model
//
//  Created by Maciej Czupryna on 19.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

public struct MediaEntity {
    public let trackId: Id
    public let trackName: String
    public let artistName: String
    public let artworkUrl100: URL?
    public let artworkUrl600: URL?
    public let genres: [String]?
    public let releaseDate: Date?

    public init(
        trackId: Id,
        trackName: String,
        artistName: String,
        artworkUrl100: URL?,
        artworkUrl600: URL?,
        genres: [String]?,
        releaseDate: Date?
    ) {
        self.trackId = trackId
        self.trackName = trackName
        self.artistName = artistName
        self.artworkUrl100 = artworkUrl100
        self.artworkUrl600 = artworkUrl600
        self.genres = genres
        self.releaseDate = releaseDate
    }
}
