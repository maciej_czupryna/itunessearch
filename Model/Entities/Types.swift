//
//  Types.swift
//  Model
//
//  Created by Maciej Czupryna on 19.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

public typealias Id = Int64
