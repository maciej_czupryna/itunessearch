//
//  ApiQuery+URLQueryItems.swift
//  ITunesApi
//
//  Created by Maciej Czupryna on 19.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import Api

extension ApiQuery {
    var queryItems: [URLQueryItem] {
        return [
            URLQueryItem(name: "term", value: term),
            URLQueryItem(name: "country", value: country),
            URLQueryItem(name: "media", value: media),
            URLQueryItem(name: "entity", value: entity)
        ]
    }
}
