//
//  MediaEntity+JSONDecodable.swift
//  ITunesApi
//
//  Created by Maciej Czupryna on 20.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import Model

extension MediaEntity: JSONDecodable {

    static func decode(_ json: [String : Any]) throws -> MediaEntity {
        guard
            let trackId = json["trackId"] as? Id,
            let trackName = json["trackName"] as? String,
            let artistName = json["artistName"] as? String
        else {
            throw ApiError.JSONDecodingError
        }

        let artworkUrl100 = URL(string: (json["artworkUrl100"] as? String) ?? "")
        let artworkUrl600 = URL(string: (json["artworkUrl600"] as? String) ?? "")
        let genres = json["genres"] as? [String]
        let releaseDate = DateFormatters.Api.releaseDateFormatter.date(from: (json["releaseDate"] as? String) ?? "")

        return MediaEntity(trackId: trackId, trackName: trackName, artistName: artistName, artworkUrl100: artworkUrl100, artworkUrl600: artworkUrl600, genres: genres, releaseDate: releaseDate)
    }
}
