//
//  SearchResponse+JSONDecodable.swift
//  ITunesApi
//
//  Created by Maciej Czupryna on 20.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import Model

extension SearchResponse: JSONDecodable {
    static func decode(_ json: [String : Any]) throws -> SearchResponse {
        guard
            let resultCount = json["resultCount"] as? Int,
            let results = json["results"] as? Array<[String : Any]>
        else {
            throw ApiError.JSONDecodingError
        }

        var entities = [MediaEntity]()
        for jsonObj in results {
            let entity = try MediaEntity.decode(jsonObj)
            entities.append(entity)
        }

        return SearchResponse(resultCount: resultCount, results: entities)
    }
}
