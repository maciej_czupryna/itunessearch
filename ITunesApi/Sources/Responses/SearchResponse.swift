//
//  SearchResponse.swift
//  ITunesApi
//
//  Created by Maciej Czupryna on 20.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import Model

struct SearchResponse {
    let resultCount: Int
    let results: [MediaEntity]
}
