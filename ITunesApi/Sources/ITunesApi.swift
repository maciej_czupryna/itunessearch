//
//  ITunesApi.swift
//  ITunesApi
//
//  Created by Maciej Czupryna on 19.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift
import Api
import Model
import Alamofire

public final class ITunesApi: Api {

    private let baseUrlString = "https://itunes.apple.com/search"

    public init() {
    }

    public func search(_ query: ApiQuery) -> Observable<[MediaEntity]> {
        guard var urlComponents = URLComponents(string: baseUrlString) else {
            fatalError("Invalid url string")
        }
        urlComponents.queryItems = query.queryItems

        guard let url = urlComponents.url else {
            fatalError("Invalid url string")
        }

        return Observable.create({ (observer) -> Disposable in
            Alamofire.request(url).responseJSON { (response) in
                switch response.result {
                case .success(let json):
                    guard
                    let json = json as? [String : Any],
                    let response = try? SearchResponse.decode(json)
                    else {
                        observer.onError(ApiError.ApiJSONParseError)
                        return
                    }
                    print(json)
                    observer.onNext(response.results)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        })
    }

    public func image(_ url: URL) -> Observable<UIImage> {
        return Observable.create({ (observer) -> Disposable in
            Alamofire.request(url).responseData(completionHandler: { (response) in
                switch response.result {
                case .success(let data):
                    guard let image = UIImage(data: data) else {
                        observer.onError(ApiError.ApiNetworkError)
                        return
                    }
                    observer.onNext(image)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            })
            return Disposables.create()
        })
    }
}
