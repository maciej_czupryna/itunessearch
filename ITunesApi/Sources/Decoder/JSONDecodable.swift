//
//  JSONDecodable.swift
//  ITunesApi
//
//  Created by Maciej Czupryna on 20.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift

protocol JSONDecodable {
    static func decode(_ json: [String: Any]) throws -> Self
}
