//
//  ApiError.swift
//  ITunesApi
//
//  Created by Maciej Czupryna on 20.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

public enum ApiError: Error {
    case ApiNetworkError
    case ApiJSONParseError
    case ApiEntityParseError
    case JSONDecodingError
}
