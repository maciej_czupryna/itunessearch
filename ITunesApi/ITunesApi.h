//
//  ITunesApi.h
//  ITunesApi
//
//  Created by Maciej Czupryna on 10.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ITunesApi.
FOUNDATION_EXPORT double ITunesApiVersionNumber;

//! Project version string for ITunesApi.
FOUNDATION_EXPORT const unsigned char ITunesApiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ITunesApi/PublicHeader.h>


