//
//  ApiQuery.swift
//  Api
//
//  Created by Maciej Czupryna on 19.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation

public struct ApiQuery {
    public var term: String
    public var country: String
    public var media: String?
    public var entity: String?

    public init(
        term: String,
        country: String,
        media: String? = nil,
        entity: String? = nil) {

        self.term = term
        self.country = country
        self.media = media
        self.entity = entity
    }
}
