//
//  ApiProtocol.swift
//  Api
//
//  Created by Maciej Czupryna on 19.10.2017.
//  Copyright © 2017 The Software House. All rights reserved.
//

import Foundation
import RxSwift
import Model

public protocol Api {
    func search(_ query: ApiQuery) -> Observable<[MediaEntity]>
    func image(_ url: URL) -> Observable<UIImage>
}
